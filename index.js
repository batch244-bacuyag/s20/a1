	// 1.
	 let number = Number(prompt("Please provide a number"));
		console.log("The number you provided is " + number);	

		for (number; number >= 0; number--){
			if(number % 10 === 0 && number > 50){
				
				console.log("The number is divisible by 10. Skipping the Number.");

				number = number-5;

				if(number % 5 === 0){
					console.log(number);
				}
				continue;
			} 

			if(number == 50){
				console.log("The current value is at " + number + ". Terminating the loop");
				break;
			}
		}		


	// 2.

		let soundOfMusic = "supercalifragilisticexpialidocious";

		function removeVowel(string){

			let consonants = "";

			let vowels = {
				'a' : true,
				'e' : true,
				'i' : true,
				'o' : true,
				'u' : true,
			};

			for (let i = 0; i < string.length; i++){

				let letter = string[i].toLowerCase();
				
				if (!vowels[letter]) {
	            consonants += string[i];
				}	
			}
			return consonants;
		}

		console.log(soundOfMusic);
		console.log(removeVowel(soundOfMusic));

